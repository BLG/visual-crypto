#!/usr/bin/env python3

import sys, os
import numpy as np
from PIL import Image


def load_img(filename):
    return ~np.asarray(Image.open(filename).convert('1'))

def save_img(img, filename):
    Image.fromarray(~img).save(filename)


def anim(img1, img2):
    h,w = img1.shape
    TMP = '/tmp/visual_anim'
    os.system(f'mkdir -p {TMP}')
    fcnt = 0
    for x2 in range(w,-1,-2):
        fimg = np.zeros((h,2*w), dtype=np.bool_)
        fimg[:,:w] |= img1
        fimg[:,x2:x2+w] |= img2
        save_img(fimg, f'{TMP}/frame{fcnt:06d}.gif')
        fcnt += 1
    os.system(f'gifsicle -O3 -d3 {TMP}/frame*.gif > anim.gif')
    os.system(f'rm -rf {TMP}')


if __name__=='__main__':
    t1 = load_img(sys.argv[1])
    t2 = load_img(sys.argv[2])
    anim(t1, t2)
