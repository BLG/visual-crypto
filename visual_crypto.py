#!/usr/bin/env python3

import argparse, os.path
import secrets
import numpy as np
from PIL import Image


PATTERNS = {'H': (((0,0),(1,1)), ((1,1),(0,0))),
            'V': (((0,1),(0,1)), ((1,0),(1,0))),
            'D': (((0,1),(1,0)), ((1,0),(0,1)))}

def gen_collection(modes):
    global COLLECTION
    # /!\ Ordering of the collection not arbitrary
    #     complementary patterns differ only by parity bit
    COLLECTION = []
    for c in modes.upper():
        if c in PATTERNS:
            COLLECTION += PATTERNS[c]
    COLLECTION = np.array(COLLECTION, dtype=np.bool_)


def share(img):
    rng = np.random.default_rng(secrets.randbits(128))
    pad = rng.integers(0,len(COLLECTION), size=img.shape, dtype=np.uint8)
    ximg = img^pad
    img1 = np.concatenate(np.concatenate(COLLECTION[pad], axis=1), axis=1)
    img2 = np.concatenate(np.concatenate(COLLECTION[ximg], axis=1), axis=1)
    return (img1, img2)

def merge(img1, img2):
    return img1 | img2


def load_img(filename):
    return ~np.asarray(Image.open(filename).convert('1'))

def save_img(img, filename, xfactor=1):
    if xfactor<=1:
        Image.fromarray(~img).save(filename)
    else:
        h,w = img.shape
        Image.fromarray(~img).resize((xfactor*w,xfactor*h)).save(filename)


def main():
    parser = argparse.ArgumentParser(description='Generate two visual cryptography transparencies from an image.')
    parser.add_argument('imgfile', help='path to image file')
    parser.add_argument('--patterns', '-p', help='set of patterns to use, string made of: D for diagonal (default), H for horizontal, V for vertical', default='D')
    parser.add_argument('--superimpose', '-s', action='store_true', help='also generate the superimposed result')
    parser.add_argument('--flip', '-f', action='store_true', help='flip colors')
    parser.add_argument('--xfactor', '-x', type=int, help='resize output image by an integer factor', default=1)
    args = parser.parse_args()
    assert 0 < args.xfactor

    gen_collection(args.patterns)

    img = load_img(args.imgfile)
    if args.flip:
        img = ~img
    img1, img2 = share(img)

    rootname = os.path.splitext(args.imgfile)[0]
    save_img(img1, rootname+'_transp1.png', args.xfactor)
    save_img(img2, rootname+'_transp2.png', args.xfactor)
    if args.superimpose:
        sup = merge(img1, img2)
        save_img(sup, rootname+'_superp.png', args.xfactor)

if __name__=='__main__':
    main()
