# Visual Crypto

Implémentation Python/Numpy/Pillow du cas le plus élémentaire (mais le plus utile en pratique) de cryptographie visuelle. À partir d'une image en noir et blanc à dissimuler, on fabrique deux transparents tels que :
 - leur superposition révèle l'image cachée ;
 - chacun des transparents, seul, ne révèle aucune information sur l'image cachée.

![animation](aux/anim.gif)

### Références

* Naor & Shamir, [_Visual cryptography_](https://www.wisdom.weizmann.ac.il/~naor/PAPERS/vis.pdf), Eurocrypt 1994.
